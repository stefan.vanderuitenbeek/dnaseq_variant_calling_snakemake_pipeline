((nil . ((projectile-project-run-cmd . "snakemake --use-conda --cores 4")
         (projectile-project-test-cmd . "snakemake -np --cores 40 --forceall"))))
