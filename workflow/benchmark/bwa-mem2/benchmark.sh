#!/usr/bin/env bash

# conda activate bam

if [ ! -f ./purged_hifiasm_assembly_v1.fasta.bwt.2bit.64 ]; then
    bwa-mem2 index ./purged_hifiasm_assembly_v1.fasta
fi

CMD="bwa-mem2 mem ./purged_hifiasm_assembly_v1.fasta ./LM2_AH5JT7AFX3_S1_L001_R1_001.fastq.gz ./LM2_AH5JT7AFX3_S1_L001_R2_001.fastq.gz"
# eval ""$CMD -r 8 > /dev/null"

hyperfine -w 1 -r 2 "$CMD -t 1" "$CMD -t 2" "$CMD -t 3" "$CMD -t 4" "$CMD -t 6" "$CMD -t 8"
