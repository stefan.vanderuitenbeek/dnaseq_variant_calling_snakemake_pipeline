#!/usr/bin/env bash

NAME=vcf

conda deactivate
conda env remove -n $NAME

# Explicit: bcftools vcftools snpEff python
mamba create -yn $NAME bcftools vcftools snpEff python
conda activate $NAME
conda env export > "$NAME".yml
