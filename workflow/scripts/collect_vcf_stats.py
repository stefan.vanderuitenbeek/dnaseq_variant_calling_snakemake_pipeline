#!/usr/bin/env python3

import pandas as pd
import gzip
import numpy as np

# Functions
def read_counts(path):
    # Find the number of allele count cols
    maxAllele = max(pd.read_table(
        path,
        usecols = ["N_ALLELES"],
        index_col = False,
        squeeze = True
    ))

    # With the number of max cols known we can ensure all are loaded
    myCols = ["n_alleles", "n_chr"] + ["counts" + str(i + 1) for i in range(maxAllele)]
    nCols = len(myCols)
    chrom = []
    pos = []
    data = []
    with gzip.open(countsFile, "rt") as f:
        f.readline()
        for line in f:
            splits = line.rstrip("\n").split("\t")

            chrom.append(splits[0])
            pos.append(int(splits[1]))

            splits = [int(x) for x in splits[2:]]
            n = len(splits)
            if n < nCols:
                splits = splits + [np.nan] * (nCols - n)
            data.append(dict(zip(myCols, splits)))

    counts = pd.DataFrame(
        data,
        index = pd.MultiIndex.from_arrays([chrom, pos], names = ("chrom", "pos"))
    )
    return counts

def compute_mac(row):
    row = row[2:]
    row = row.sort_values(ascending = False)
    if pd.isna(row[1]):
        return {"n_biallelic": row[0], "mac":row[1]}
    return {"n_biallelic": row[0] + row[1], "mac":row[1]}


if __name__ == "__main__":
    # Input files
    statsFile = "results/vcf_stats/site_info.tsv.gz"
    countsFile = "results/vcf_stats/site_allele_counts.tsv.gz"
    siteDpFile = "results/vcf_stats/site_mean_depth.tsv.gz"
    siteMissFile = "results/vcf_stats/site_missing.tsv.gz"
    sampleDpFile = "results/vcf_stats/sample_depth.tsv"
    sampleMissFile = "results/vcf_stats/sample_missing.tsv"
    # sampleHetFile = "results/vcf_stats/sample_heterozygosity.tsv"

    # Output files
    siteOutFile = "results/site_stats.tsv.gz"
    sampleOutFile = "results/sample_stats.tsv"

    # Site Statistics ------
    site = pd.read_table(
        statsFile,
        header = None,
        comment = "#",
        na_values = ".",
        names = ["chrom", "pos", "indel", "qual", "dp", "mq", "rpbz", "mqbz", "bqbz"],
        index_col = ["chrom", "pos"]
    )
    site["indel"] = [not pd.isna(x) for x in site["indel"]]

    siteDp = pd.read_table(
        siteDpFile,
        header = 0,
        names = ["chrom", "pos", "mean_dp", "var_dp"],
        index_col = ["chrom", "pos"]
    )

    siteMiss = pd.read_table(
        siteMissFile,
        header = 0,
        names = ["chrom", "pos", "n", "n_filter", "n_missing", "frac_missing"],
        index_col = ["chrom", "pos"]
    )

    counts = read_counts(countsFile)

    mac = counts.apply(compute_mac, axis = 1, result_type = "expand")

    # Join
    site = site.join(siteDp["mean_dp"])
    site = site.join(siteMiss["frac_missing"])
    site = site.join(counts["n_chr"])
    site = site.join(mac)

    # Write to file
    site.to_csv(siteOutFile, sep = "\t")


    # Sample Statistics ------
    sample = pd.read_table(
        sampleDpFile,
        header = 0,
        names = ["name", "n", "mean_dp"],
        index_col = "name"
    )
    del sample["n"]

    sampleMiss = pd.read_table(
        sampleMissFile,
        header = 0,
        names = ["name", "n", "n_filtered", "n_missing", "frac_missing"],
        index_col = "name"
    )

    sample = sample.join(sampleMiss["frac_missing"])

    # Write to file
    sample.to_csv(sampleOutFile, sep = "\t")
