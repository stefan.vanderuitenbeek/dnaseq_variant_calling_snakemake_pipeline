#!/usr/bin/env python3

import argparse
import gzip
import os.path
import sys

# Delimiter for output file
delim = "\t"
# test = "../results/alignment_stats/Gp_Newton.tsv ../results/alignment_stats/Re167.tsv ../results/alignment_stats/Hamachuco.tsv -d ../results/alignment_stats/read_depth.tsv.gz -o alignment_stats.tsv"

# Argparse
parser = argparse.ArgumentParser()
parser.add_argument("stat", nargs="+", help="samtools flagstat files to combine")
parser.add_argument("-d", "--depth", help="output from samtools depth -Ha containing all samples from STAT")
parser.add_argument("-m", "--minDP", default=1, help="minimum read depth to consider a base covered")
parser.add_argument("-o", "--out", help="name for tab-separated output file")
xargs = parser.parse_args()
# xargs = parser.parse_args(test.split(" "))

# Functions
def GetName(x):
    noExt = os.path.splitext(x)[0]
    return os.path.split(noExt)[1]

# Main
results = {}
sys.stderr.write("Collecting statistics...")
for current in xargs.stat:
    # Create name from file name
    name = GetName(current)
    # Load
    with open(current, "r") as f:
        stats = f.readlines()
    # Parse
    stats = [x.split("\t")[1].strip("\n") for x in stats]
    total = stats[2]
    mapped = stats[6]
    paired = stats[7]
    baseCov = stats[20]
    mismatch = stats[23]
    avgLen = stats[25]
    avgBQ = stats[31]
    # Store
    results[name] = [total, mapped, paired, baseCov, mismatch, avgLen, avgBQ]
columns = ["read_count", "reads_mapped", "reads_paired", "bases_mapped", "mismatches", "avg_lenth", "avg_BQ"]

if xargs.depth is not None:
    sys.stderr.write("Computing coverage...")
    with gzip.open(xargs.depth, "rt") as f:
        # Parse sample names from header
        names = f.readline()[1:].strip("\n").split("\t")[2:]
        names = [GetName(x) for x in names]
        # Precompute range for speedup
        nSample = len(names)
        sampleRange = range(nSample)
        # Loop over each of the sites and sum read depth per sample
        cov = [0] * nSample
        for n, line in enumerate(f):
            line = line.strip("\n").split("\t")[2:]
            for i in sampleRange:
                if int(line[i]) >= xargs.minDP:
                    cov[i] += 1
    # Divide by total and add to results
    for i in sampleRange:
        results[names[i]].append(str(cov[i] / n))
    columns += ["coverage"]

# Write to file
sys.stderr.write("Writing to file...")
with open(xargs.out, "w") as f:
    # Header
    columns = ["name"] + columns
    f.write(delim.join(columns) + "\n")
    # Values
    for name, stats in results.items():
        f.write(delim.join([name] + stats) + "\n")
