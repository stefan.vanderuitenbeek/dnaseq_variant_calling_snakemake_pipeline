#!/usr/bin/env python3

# REVIEW is there a need to parse other snpEff fields (LOF & NMD)

import argparse
import gzip

# import ipdb
# ipdb.set_trace()

def open_file(path, mode = "rt"):
    if mode.startswith("w") and path.endswith(".bgz"):
        raise Exception("Writing to Blocked-Gzip format is not supported!")
    if path.endswith(".gz") or path.endswith(".bgz"):
        return gzip.open(path, mode)
    return open(path, mode)

def skip_header(vcf):
    for line in vcf:
        if line.startswith("##"):
            continue
        break

    if not line.startswith("#CHROM\tPOS\t"):
        raise Exception("The first two columns of the vcf should be CHROM and POS!")
    if not line.endswith("\tINFO\n"):
        raise Exception("Vcf should only contain fixed fields! Use 'bcftools view -G'.")

def write_col_names(file):
    col_names = [
        "chrom",
        "pos",
        "allele",
        "annotation",
        "impact",
        "gene_name",
        "gene_id",
        "feature_type",
        "feature_id",
        "biotype",
        "rank_total",
        "hgvs.c",
        "hgvs.p",
        "cdna_loc",
        "cds_loc",
        "prot_loc",
        "distance",
        "error_code"
    ]
    file.write("\t".join(col_names) + "\n")

def get_info(info, what):
    for field in info.split(";"):
        if field.startswith(what + "="):
            return field.partition("=")[2]
    raise Exeption(f"The key '{what}' could not be found in the INFO column!")

def parse_snpEff(in_path, out_path):
    vcf = open_file(in_path)
    out = open_file(out_path, "wt")

    skip_header(vcf)

    write_col_names(out)

    for line in vcf:
        fields = line.strip("\n").split("\t")
        location = fields[0] + "\t" + fields[1]
        annotations = get_info(fields[-1], "ANN").split(",")

        # Write one annotation per line
        for annotation in annotations:
            annotation = annotation.replace("|", "\t")
            out.write(location + "\t" + annotation + "\n")

    vcf.close()
    out.close()

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("input",
                        help="Input vcf name, gzipped vcfs should in .gz or .bgz")
    parser.add_argument("output",
                        help="Output file name (TSV), zipped files should end with .gz")
    xargs = parser.parse_args()

    parse_snpEff(xargs.input, xargs.output)
