#!/usr/bin/env python3

import argparse
import gzip

# import ipdb
# ipdb.set_trace()

def cat(x, file = None):
    if file is None:
        print(x, end = "")
    else:
        file.write(x)

def open_file(path, mode = "rt"):
    if mode.startswith("w") and path.endswith(".bgz"):
        raise Exception("Writing to Blocked-Gzip format is not supported!")
    if path.endswith(".gz") or path.endswith(".bgz"):
        return gzip.open(path, mode)
    return open(path, mode)

def convert_vcf_header(in_file, out_file = None, skip_meta = False):
    for line in in_file:
        if line.startswith("##"):
            if not skip_meta:
                cat(line, out_file)
            continue
        if line.startswith("#"):
            cat(line[1:], out_file)
            break
        raise Exception("Non meta line detected in header")

def convert_tsv_header(in_file, out_file = None):
    for line in in_file:
        if line.startswith("##"):
            cat(line, out_file)
            continue
        if line.startswith("#"):
            raise Exception("VCF header line already present!")
        cat("#" + line, out_file)
        break

def vcf2tsv(in_path, out_path = None, skip_meta = False, reverse = False):
    in_file = open_file(in_path)
    if out_path is not None:
        out_file = open_file(out_path, "wt")
    else:
        out_file = None

    if reverse:
        convert_tsv_header(in_file, out_file)
    else:
        convert_vcf_header(in_file, out_file, skip_meta)

    for line in in_file:
        cat(line, out_file)

    in_file.close()
    if out_path is not None:
        out_file.close()

if __name__ == "__main__":
    # Create CLI
    parser = argparse.ArgumentParser()
    parser.add_argument("input",
                        help="Input file name, zipped files should end with .gz or .bgz")
    parser.add_argument("-s", "--skip", action="store_true",
                        help="Skip printing header lines starting with # (TSV only)")
    parser.add_argument("-r", "--reverse", action="store_true",
                        help="Convert a tsv back to a VCF. Does not create a header if missing.")
    parser.add_argument("-o", dest="output",
                        help="Output file name [stdout]. Zipped files should end with .gz.")
    xargs = parser.parse_args()

    vcf2tsv(xargs.input, xargs.output, xargs.skip, xargs.reverse)
