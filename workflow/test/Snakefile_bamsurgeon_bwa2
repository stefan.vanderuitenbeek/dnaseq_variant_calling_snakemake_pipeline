#defining wildcards for samples, reference genome and annotation in configuration file
configfile: "config.yaml"


#define the output, if you have multiple samples use expand function
rule all: 
    input:
        expand("mutated_reads/{sample}.bam", \
              sample=glob_wildcards(config["samples"]).sample)


#indexing the reference genome
rule bwa_index:
    input:
        config["genome"]
    output:
        expand("{genome}.{suffix}", genome = config["genome"], \
               suffix = ["amb", "ann", "pac"])
    params:
        prefix=config["genome"]
    log:
        "logs/bwa2_index/Index.log"
    conda:
        "envs/bwa2_index.yaml"
    shell:
        "(bwa-mem2 index {input} -p {params.prefix}) 2> {log}"


#index the sorting alignments
rule samtools_index:
    input:
        config["samples"]
    output:
        "sorted_reads/{sample}.bam.bai"
    conda:
        "envs/samtools.yaml"
    threads: 10
    shell:
        "samtools index -@ {threads} {input}"


#get random amount of reads from bamfile to mutate (in a file between 1 or 2 GB of reads) 0.01% of all reads
rule samtools_view:
    input:
        config["samples"]
    output:
        "sorted_reads/{sample}_0.001%_of_reads.bam"
    conda:
        "envs/samtools.yaml"
    threads: 10
    shell:
        "samtools view -s 0.00001 -b {input} -h -o {output}"


#transform to bed to check and get the positions of reads in the bamfiles
rule bedtools_bamtobed:
    input:
       "sorted_reads/{sample}_0.001%_of_reads.bam"
    output:
       "sorted_reads/{sample}_0.001%_of_reads.bed"
    threads: 10
    shell:
        "bedtools bamtobed -i {input} > {output}"


#first do the above rules, decide how many positions you want to change and after that 'open' this rule with removing all #
#mutate bam files at random position made with bedtools
#make VAR file in excel 
rule addsnv_bam:
    input:
        bam=config["samples"],
        genome=expand("{genome}.{suffix}", genome = config["genome"], \
               suffix = ["amb", "ann", "pac"]),
        var="VAR_files/{sample}_0.001%_of_reads.bed",
        bai="sorted_reads/{sample}.bam.bai"
    output:
        "mutated_reads/{sample}.bam"
    params:
        prefix=config["genome"]
    log:
        "logs/addsnv/{sample}.log"
    conda:
        "envs/bwa_mem2_samtools.yaml"
    threads: 8
    shell:
        "(addsnv.py -f {input.bam} -v {input.var} -r {params.prefix} -m 0.5 -o {output}) 2> {log}"
